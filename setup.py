from setuptools import setup, find_packages

base_dependencies = [
    "click >= 7.1.2",
    "requests",
    "pandas"
]
dev_dependencies = [
    "flake8>=3.8.4",
    "mypy>=0.8.0",
    "pytest",
    "hypothesis",

]

setup(
    name="bandits",
    packages=find_packages("src"),
    package_dir={"": "src"},
    author="Ismael Cabral, Rick Vergunst",
    install_requires=base_dependencies,
    extras_require={
        "dev": dev_dependencies
    },
    entry_points={
        'console_scripts': [
            'bandits = bandits.cli:main'
        ]
    }
)
