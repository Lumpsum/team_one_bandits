import requests
import pandas as pd
import pickle
import os
import numpy as np


class Requester:
    def __init__(self, usr: str, pwd: str, local=False):
        """
        Makes the requests
        :param usr: is the username
        :param pwd: is the password
        """
        self.usr = usr
        self.pwd = pwd
        self.url = f"" if local else f"http://145.53.80.207:8081/"

    def pull_arm(self, bandit: str, arm: str):
        """
        pull arm and return result
        :param bandit: str of chosen bandit
        :param arm: str of chosen arm
        :return: json of the pull
        """
        r = requests.post(f"{self.url}/pull/{bandit}/{arm}", json={
            'user': self.usr,
            'password': self.pwd
        }).json()
        return r

    def get_state(self):
        """
        retrieve state of game
        :return: json of the state
        """
        r = requests.get(f"{self.url}/state").json()
        return r

    def get_default_bandits(self):
        """
        retrieve default bandits
        :return: bandit result in json
        """
        r = requests.get(f"{self.url}").json()
        return r


class Pickle():
    def __init__(self):
        """
        Set file_path for pickle
        :param file_path: paht to pickle
        """
        self.request = Requester("teamone", "caDOMFUSEv")

    @staticmethod
    def retrieve_pickle(file_path: str, bandit: str, arm: str):
        """
        create or retrieve pickl file at given path
        :param file_path: path to write or read from
        :param bandit: bandit to use
        :param arm: arms present on the bandit
        :return: pkl file
        """
        try:
            with open(file_path, 'rb') as f:
                pkl = pickle.load(f)
        except IOError as e:
            df = pd.DataFrame([(bandit, x) for x in arm], columns=['bandit', 'arm'])
            pkl = pd.concat([df, pd.DataFrame(columns=list(['total_payout', 'count', 'ppp']))]).fillna(0)
            pkl.to_pickle(file_path)

        return pkl

    @staticmethod
    def write_to_pickle(path: str, content: pd.DataFrame):
        """
        write to existing pickle
        :param path: path to write to
        :param content: contains the new dataframe
        """
        try:
            content.to_pickle(path)
        except Exception as e:
            raise Exception(e)


class PullArm:
    """
    Abstract class for pullin an Arm

    """
    def select_arm(self):
        ...


class GeneticAlgoritm:

    """
    This code uses a GA approach to the Arm-Bandit problem
    Deprecated due to low tournament size

    """

    def __init__(self, tournament_size=4):
        self.tournament_size = tournament_size

    def select_arm(self, scores):
        selection = scores.sample(self.tournament_size)

        tournament = selection.sort_values('ppp', ascending=False)

        selected = tournament[tournament.ppp == tournament.ppp.iat[0]].sample(1).to_dict('records')[0]

        return {"bandit": selected['bandit'], "arm": selected['arm']}


class RandomChoice:
    """
    This algorithm selects a random element of a bandit
    This can be used a base case

    """

    def __init__(self):
        pass

    def select_arm(self, scores):
        """
        Method to select a random arm from a bandit

        """
        selected = scores.sample(1).to_dict('records')[0]

        return {"bandit": selected['bandit'], "arm": selected['arm']}


class EpsilonGreedy():

    """
    Exploit - Exploit strategy to the Armed-Bandit problem

    """


    def __init__(self):
        ...

    def select_arm(self, scores):
        """
        Method to select an arm from a bandit
        :param  scores: scores of an specific bandit

        """

        epsilon = (3500 - scores['count'].sum()) / 3500

        if epsilon < 0:
            epsilon = 0.01

        explore = np.random.binomial(1, epsilon)

        if explore == 1:
            selected = scores.sample(1).to_dict('records')[0]
        else:
            ordered = scores.sort_values('ppp', ascending=False)

            selected = ordered[ordered.ppp == ordered.ppp.iat[0]].sample(1).to_dict('records')[0]

        return {"bandit": selected['bandit'], "arm": selected['arm']}