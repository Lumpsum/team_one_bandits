import click
import logging
from bandits.orchestrator import Orchestrator


@click.group()
@click.option("--verbose", is_flag=True)
def main(verbose):
    """
    Library to learn and find the best bandits strategy

    :param verbose: Define log level
    :return: None
    """
    level = logging.DEBUG if verbose else logging.INFO
    logging.basicConfig(level=level)


@main.command()
@click.argument('algorithm')
@click.argument('pulls')
def pull(algorithm, pulls):
    orchestrator = Orchestrator(algorithm)
    orchestrator.generate_pulls(pulls)

if __name__ == '__main__':
    main()
