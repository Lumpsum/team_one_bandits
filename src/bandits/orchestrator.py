from bandits.backend import Requester, Pickle, GeneticAlgoritm, RandomChoice, PullArm, EpsilonGreedy
import os
import logging
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'


class Orchestrator:
    def __init__(self, alg: str):
        """
        Orchestrate the pulling of the arms
        :param alg: contain the chosen algorithm
        """
        self.usr = "teamone"
        self.request = Requester(self.usr, "caDOMFUSEv")
        self._check_dir()
        self.alg = alg
        self.pkl = Pickle()
        self.alg_class: PullArm = self._pick_algorithm(alg)
        self.remaining_coins = self._remaining_coin_amount()

    def generate_pulls(self, pulls: int):
        """
        Pulls for the given amount
        :param pulls: amount of pulls
        """
        logging.info(f"Doing %s pulls", pulls)
        bandits = self.request.get_default_bandits()

        for bandit, arm in bandits.items():
            path = f"./data/{self.alg}-{bandit}"
            file = self.pkl.retrieve_pickle(path, bandit, arm)
            for pull in range(1, int(pulls)+1):
                bandit_r = self.alg_class.select_arm(file)
                arm_r = self.request.pull_arm(bandit_r['bandit'], bandit_r['arm'])
                row = file.loc[file.arm == bandit_r['arm']]
                row.loc[:, 'total_payout'] += arm_r['value']
                row.loc[:, 'count'] += 1
                row.loc[:, 'ppp'] = row['total_payout'] / row['count']
                file.loc[
                    (file.arm == bandit_r['arm'])
                    ] = row
                amount = self._subtract_coin(bandit)
                if amount < 1:
                    logging.info(f"no more coins for bandit %s", bandit)
                    break

            self.pkl.write_to_pickle(path, file)
            logging.info("Results written to %s", path)

        logging.info(f"All %s pulls succeeded for all bandits", pulls)

    def _remaining_coin_amount(self) -> int:
        """
        Find coin amount
        :return: Return coin amount in int
        """
        r = self.request.get_state()
        return r

    def _subtract_coin(self, bandit: str):
        """
        subtract coin from total
        :param bandit: bandit to subtract from
        :return: returns coin amount
        """
        self.remaining_coins[bandit][self.usr]['coins'] = \
            int(self.remaining_coins[bandit][self.usr]['coins']) - 1
        return self.remaining_coins[bandit][self.usr]['coins']

    @staticmethod
    def _check_dir():
        """
        Check and/or create data folder
        """
        if not os.path.isdir("./data"):
            os.mkdir("./data")

    @staticmethod
    def _pick_algorithm(alg):
        """
        Choose algorithm based on given
        :param alg: contain the chosen algorithm
        :return: algorithm class
        """
        if alg == 'gen':
            alg_choice = GeneticAlgoritm()
        elif alg == 'random':
            alg_choice = RandomChoice()
        elif alg == 'eps':
            alg_choice = EpsilonGreedy()
        else:
            raise ValueError(f"{alg} is not a valid choice")

        return alg_choice
